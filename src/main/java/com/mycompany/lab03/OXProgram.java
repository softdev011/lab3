/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab03;

/**
 *
 * @author growt
 */
class OXProgram {

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)) {
            return true;
        }
        if (checkCol(table, currentPlayer)) {
            return true;
        }
        
        if(checkDiagonalRight(table,currentPlayer)) {
            return true;
        }
        if(checkDiagonalLetf(table,currentPlayer)) {
            return true;
        }
        if(checkDraw(table,currentPlayer)) {
            return false;
        }
        
        return false;
       }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row<3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }
    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col<3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkDraw(String[][] table, String currentPlayer) {
        for (int row = 0; row<3; row++) {
            if (checkDraw(table, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }
    
    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    
    }
    
    private static boolean checkDraw(String[][] table,int row) {
        return table[row][0].equals("-") || table[row][1].equals("-") || table[row][2].equals("-");
    }
    

    private static boolean checkDiagonalRight(String[][] table, String currentPlayer) {
        return table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer);
    }

    
    private static boolean checkDiagonalLetf(String[][] table, String currentPlayer) {
        return table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer);
    }



}
